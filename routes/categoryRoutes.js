const express = require('express')
const { getAllCategories, getCategoryById, addCategory, updateCategory, deleteCategory } = require('../controllers/categoryController')
const router = express.Router()

router.get('/', getAllCategories)

router.get('/:categoryId', getCategoryById)

router.post('/', addCategory)

router.patch('/:categoryId', updateCategory)

router.delete('/:categoryId', deleteCategory)

module.exports = router